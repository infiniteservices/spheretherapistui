module.exports =  {
    singleQuote:  true,
    printWidth:  150,
    tabWidth:  4,
    semi: true
  };